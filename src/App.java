class Counter implements Runnable {  
    private int c = 0;  
    public void increment()   
    {  
        try {  
            Thread.sleep(100);  
        }   
        catch (InterruptedException e) {  
            e.printStackTrace();  
        }  
        c++;  
    }  
    public void decrement() {      
        c--;  
    }  
    public int getValue() {  
        return c;  
    }  
    @Override  
    public void run() {
        //si lo sincronizamos se soluciona el race condition
        //synchronized(this){  
            this.increment();  
            System.out.println("valor del hilo despues de incrementar " + Thread.currentThread().getName() + " " + this.getValue());
            this.decrement();  
            System.out.println("ultimo valor del hilo " + Thread.currentThread().getName() + " " + this.getValue());  
     //}        
    }  
}  
public class App {  
    public static void main(String args[]) {  
        Counter counter = new Counter();  
        Thread t1 = new Thread(counter, "Hilo-1");  
        Thread t2 = new Thread(counter, "Hilo-2");  
        Thread t3 = new Thread(counter, "Hilo-3");  
        t1.start();  
        t2.start();  
        t3.start();  
    }      
}  